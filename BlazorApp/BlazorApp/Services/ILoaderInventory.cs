﻿using System;
using BlazorApp.Models;

namespace BlazorApp.Services
{
	public interface ILoaderInventory
	{
		List<SavableItem> LoadItems();
	}
}

