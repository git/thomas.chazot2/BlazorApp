﻿using System;
using BlazorApp.Models;

namespace BlazorApp.Services
{
    public interface ISaverInventory
    {
        void SaveItems(List<SavableItem> items);
    }
}

