﻿using System;
using BlazorApp.Components;
using BlazorApp.Models;

namespace BlazorApp.Services
{
    public interface IDataService
    {
        Task Add(ItemModel model);

        Task<int> Count();
        Task<Item> GetById(int id);
        Task<List<Item>> List(int currentPage, int pageSize);
        Task Update(int id, ItemModel itemModel);
        Task Delete(int id);
        Task<List<CraftingRecipe>> GetRecipes();
        Task<List<Item>> SortedList(int currentPage, int pageSize);
        Task<(List<Item>, int)> SearchList(int currentPage, int pageSize, string recherche);
    }
}

