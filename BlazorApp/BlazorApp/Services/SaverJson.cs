﻿using System;
using BlazorApp.Data;
using System.Text.Json;
using BlazorApp.Models;

namespace BlazorApp.Services
{
	public class SaverJson : ISaverInventory
	{
        private string FileName { get; set; }

        public SaverJson()
        {
            FileName = "wwwroot/InventoryItems.json";
        }

        public void SaveItems(List<SavableItem> items)
        {
            string jsonString = JsonSerializer.Serialize(items);
            FileStream fileStream= File.Create(FileName);
            JsonSerializer.Serialize(fileStream, items);
            fileStream.Dispose();
        }
    }
}

