﻿using System;
using BlazorApp.Data;
using System.Text.Json;
using BlazorApp.Models;

namespace BlazorApp.Services
{
	public class LoaderJson : ILoaderInventory
	{

        private string FileName { get; set; }

		public LoaderJson()
		{
            FileName="wwwroot/InventoryItems.json";
		}

        public List<SavableItem> LoadItems()
        {
            using FileStream openStream = File.OpenRead(FileName);
            List<SavableItem>? items = JsonSerializer.Deserialize<List<SavableItem>>(openStream);
            return items;

        }
    }
}

