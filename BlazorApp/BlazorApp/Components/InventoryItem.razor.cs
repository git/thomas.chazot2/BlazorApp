﻿using System;
using BlazorApp.Models;
using BlazorApp.Components;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;

namespace BlazorApp.Components
{
	public partial class InventoryItem
	{
        [Parameter]
        public int Index { get; set; }

        [Parameter]
        public Item Item { get; set; }

        [Parameter]
        public int NbItem { get; set; }

        [Parameter]
        public bool NoDrop { get; set; }

        [CascadingParameter]
        public InventoryComp Parent { get; set; }

        internal void OnDragEnter()
        {
            if (NoDrop)
            {
                return;
            }

        }

        internal void OnDragLeave()
        {
            if (NoDrop)
            {
                return;
            }

        }


        /// <summary>
        /// Event that change the Item of the InventoryItem with the one that is being dropped on
        /// </summary>
        internal void OnDrop()
        {
            if (NoDrop)
            {
                return;
            }


            if (Parent.CurrentDragItem != null)
            {
                //Test if the item being dropped is the same as this item and if it this add the number of items
                if (Parent.CurrentDragItem.Equals(Item))
                {
                    if (NbItem + Parent.CurrentDragNbItem > Item.StackSize)
                    {
                        int nbTrop = NbItem + Parent.CurrentDragNbItem - Item.StackSize;
                        NbItem = Item.StackSize;
                        Parent.CurrentDragNbItem = nbTrop;
                    }
                    else
                    {
                        NbItem = NbItem + Parent.CurrentDragNbItem;
                        Parent.CurrentDragNbItem = -1; //Allow to to know on the OnDragEnd method if the sum was < to the stackSize
                    }
                }
                //If it isn't this Item will now be the item that is being dropped and will put in CurrentDragItem/Index/NbItem its informations to allow the swap
                else
                {
                    int changement = NbItem;
                    NbItem = Parent.CurrentDragNbItem;
                    Parent.CurrentDragNbItem = changement;
                }
                Item tmp = this.Item;
                this.Item = Parent.CurrentDragItem;
                Parent.CurrentDragItem = tmp;
                Parent.CurrentDragIndex = this.Index;
                Parent.InventoryItems[this.Index] = new SavableItem(this.Item, NbItem);

            }
            Parent.Actions.Add(new InventoryAction { Action = "On Drop", Item = this.Item, Index = this.Index });
        }


        /// <summary>
        /// Event that start the drag of an InventoryItem by putting in CurrentDragItem/Index/NbItem its informations 
        /// </summary>
        private void OnDragStart(MouseEventArgs e)
        {

            if (this.Item != null)
            {
                Parent.CurrentDragItem = this.Item;
                if (!NoDrop)
                {
                    //This part is for if the user drag with his right click to divide the number of item by two
                    //Sadly it doesn't work because of the context menu (the drag doesn't statt with the right click even if we prevent the context menu from showing)
                    if (e.Button == 2 && NbItem>1)
                    {
                        Parent.CurrentDragIndex = -1;
                        if (NbItem % 2 == 1)
                        {
                            Parent.CurrentDragNbItem = NbItem/2+1;
                        }
                        else
                        {
                            Parent.CurrentDragNbItem = NbItem/2;
                        }
                        NbItem = NbItem / 2;
                    }
                    //This part is for the left click
                    else
                    {
                        Parent.CurrentDragIndex = this.Index;
                        Parent.CurrentDragNbItem = NbItem;
                        this.Item = null;
                        NbItem = -1;
                        Parent.InventoryItems[Index] = null;
                    }
                }
                else
                {
                    Parent.CurrentDragIndex = -1; //Current index is equal to -1 to tell the other functions that this item was from the list that way it isn't swapped
                    Parent.CurrentDragNbItem = 1;
                }

            }
            Parent.Actions.Add(new InventoryAction { Action = "Drag Start", Item = this.Item, Index = this.Index });

        }


        /// <summary>
        /// Event to swap the dragged item with the one that it was dropped on
        /// </summary>
        internal void OnDragEnd()
        {
            //Test to prevent from dropping on the list or from dropping a empty InventoryItem
            if (NoDrop || Parent.CurrentDragItem==null)
            {
                return;
            }

            //Test if the InventoryItem is from the list with the CurrentDragIndex != -1 or if the InventoryItem is being dropped onto itself or if the item was equal to the other and the sum of their nbItem was < than the stackSize
            if (Parent.CurrentDragIndex != -1 && Parent.CurrentDragIndex!= Index && Parent.CurrentDragNbItem!=-1)
            {
                this.Item = Parent.CurrentDragItem;
                NbItem = Parent.CurrentDragNbItem;
                Parent.InventoryItems[this.Index] = new SavableItem(this.Item, NbItem);
            }

            Parent.Actions.Add(new InventoryAction { Action = "Drag End", Item = this.Item, Index = this.Index });

            Parent.CurrentDragIndex = -1;
            Parent.CurrentDragItem = null;

        }
    }
}

