﻿using System;
using BlazorApp.Models;

namespace BlazorApp.Components
{
    public class InventoryAction
    {
        public string Action { get; set; }
        public int Index { get; set; }
        public Item Item { get; set; }
    }
}

