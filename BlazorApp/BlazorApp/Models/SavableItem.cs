﻿using System;
namespace BlazorApp.Models
{
	public class SavableItem
	{

		public Item Item { get; set; }

		public int NbItem { get; set; }

		public SavableItem(Item item, int nbItem)
		{
			Item = item;
			NbItem = nbItem;
		}
	}
}

