﻿using System;
using BlazorApp.Components;
using BlazorApp.Models;
using BlazorApp.Services;
using Blazorise;
using Blazorise.DataGrid;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Localization;

namespace BlazorApp.Pages
{
	public partial class Inventory
	{
        [Inject]
        public IDataService DataService { get; set; }

        [Inject]
        public IStringLocalizer<List> Localizer { get; set; }

        public List<Item> Items { get; set; } = new List<Item>();

        private int totalItem;

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            base.OnAfterRenderAsync(firstRender);

            if (!firstRender)
            {
                return;
            }

            Items = await DataService.List(0, await DataService.Count());
            StateHasChanged();
        }


        private async Task OnReadData(DataGridReadDataEventArgs<Item> e)
        {
            if (e.CancellationToken.IsCancellationRequested)
            {
                return;
            }

            if (!e.CancellationToken.IsCancellationRequested)
            {
                Items = await DataService.List(e.Page, e.PageSize);
                totalItem = await DataService.Count();
            }
        }
    }
}