# BlazorApp


## Launching the project
So as to launch our project, you must launch both the app and the API. You can do so by defining them both as new starting projects. 

Go to Visual Studio -> Right click on the solution -> Click on definig new starting projects -> Select both the app and the API -> Save these settings -> That's it !


## Branch
Our work is on the master branch.


## Specific manipulations
You just have to follow the launching project instructions, but if it is not already done, you also have to add the API as a new project by right clicking on the solution, then add an existing project and select the API, and you're done !


## Data to be created
The only data that will be created is the items you add by the list page. A JSon file is also created so as to save your inventory in the inventory page.





**Have a great time using our BlazorApp !**